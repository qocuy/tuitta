/*
 * Copyright (c) 2014 DeNA Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <errno.h>
#include <limits.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <postgresql/libpq-fe.h>
#include <jansson.h>
#include "h2o.h"
#include "h2o/http1.h"
#include "h2o/http2.h"
#include "h2o/memcached.h"
#include "h2o/configurator.h"
#include "yoml.h"
#include "standalone.h"
#include "db.h"

#define USE_HTTPS 1
#define USE_MEMCACHED 0

static PGconn *db_conn;

static h2o_pathconf_t *register_handler(h2o_hostconf_t *hostconf, const char *path, int (*on_req)(h2o_handler_t *, h2o_req_t *))
{
    h2o_pathconf_t *pathconf = h2o_config_register_path(hostconf, path, 0);
    h2o_handler_t *handler = h2o_create_handler(pathconf, sizeof(*handler));
    handler->on_req = on_req;
    return pathconf;
}

static char *get_page(h2o_req_t *req) {
  char *page;

  if (req->query_at == SIZE_MAX) {
    page = "0";
  } else {
    int len = req->path.len - req->query_at;
    page = (char *) h2o_mem_alloc_pool(&req->pool, char, sizeof(char) * len);
    memcpy(page, req->path.base + req->query_at + 1, len - 1);
    page[len - 1] = 0;
  }

  return page;
}

static char *get_entity(h2o_req_t *req) {
  char *entity = (char *) h2o_mem_alloc_pool(&req->pool, char, req->entity.len + sizeof(char));
  memcpy(entity, req->entity.base, req->entity.len);
  entity[req->entity.len] = 0;
  return entity;
}

static json_t *tweet_to_json(PGresult *result, int i) {
  json_t *tweet = json_object();
  json_object_set_new(tweet, "id", json_string(PQgetvalue(result, i, 0)));
  json_object_set_new(tweet, "text", json_string(PQgetvalue(result, i, 1)));
  json_object_set_new(tweet, "createdAt", json_string(PQgetvalue(result, i, 2)));
  return tweet;
}

static json_t *tweets_to_json(PGresult *result) {
  int tuples = PQntuples(result);
  json_t *tweets = json_array();

  for (int i = tuples - 1; i >= 0; i--) {
    json_array_append_new(tweets, tweet_to_json(result, i));
  }

  return tweets;
}

static void send_json(h2o_req_t *req, json_t *json) {
  char *text = json_dumps(json, JSON_COMPACT);
  static h2o_generator_t generator = {NULL, NULL};

  h2o_iovec_t body = h2o_strdup(&req->pool, text, SIZE_MAX);
  req->res.status = 200;
  req->res.reason = "OK";
  h2o_add_header(&req->pool, &req->res.headers, H2O_TOKEN_CONTENT_TYPE, NULL, H2O_STRLIT("text/json"));
  h2o_start_response(req, &generator);
  h2o_send(req, &body, 1, 1);
  
  free(text);
}

static int api_timeline(h2o_handler_t *self, h2o_req_t *req) {
  const char *page = get_page(req);
  PGresult *result = tuitta_db_get_timeline(db_conn, TUITTA_DEFAULT_COUNT, page);
  json_t *tweets = tweets_to_json(result);
  send_json(req, tweets);
  json_decref(tweets);
  PQclear(result);
  return 0;
}

static int api_update(h2o_handler_t *self, h2o_req_t *req)
{
    if (!(h2o_memis(req->method.base, req->method.len, H2O_STRLIT("POST")) &&
        h2o_memis(req->path_normalized.base, req->path_normalized.len, H2O_STRLIT("/api/update")))) {
      return -1;
    }

    const char *status = get_entity(req);
    PGresult *result = tuitta_db_update_status(db_conn, status);

    json_t *tweet = tweet_to_json(result, 0);
    send_json(req, tweet);
    json_decref(tweet);
    PQclear(result);

    return 0;
}

static int api_search(h2o_handler_t *self, h2o_req_t *req) {
  const char *pattern = get_entity(req);
  const char *page = get_page(req);

  PGresult *result = tuitta_db_search(db_conn, pattern, TUITTA_DEFAULT_COUNT, page);

  json_t *tweets = tweets_to_json(result);
  send_json(req, tweets);
  json_decref(tweets);
  PQclear(result);
  return 0;
}

static int api_date(h2o_handler_t *self, h2o_req_t *req) {
  const char *date = get_entity(req);
  const char *page = get_page(req);
  PGresult *result = tuitta_db_travel(db_conn, date, TUITTA_DEFAULT_COUNT, page);

  json_t *tweets = tweets_to_json(result);
  send_json(req, tweets);
  json_decref(tweets);
  PQclear(result);
  return 0;
}

static h2o_globalconf_t config;
static h2o_context_t ctx;
static h2o_multithread_receiver_t libmemcached_receiver;
static h2o_accept_ctx_t accept_ctx;

#if H2O_USE_LIBUV

static void on_accept(uv_stream_t *listener, int status)
{
    uv_tcp_t *conn;
    h2o_socket_t *sock;

    if (status != 0)
        return;

    conn = h2o_mem_alloc(sizeof(*conn));
    uv_tcp_init(listener->loop, conn);

    if (uv_accept(listener, (uv_stream_t *)conn) != 0) {
        uv_close((uv_handle_t *)conn, (uv_close_cb)free);
        return;
    }

    sock = h2o_uv_socket_create((uv_handle_t *)conn, (uv_close_cb)free);
    h2o_accept(&accept_ctx, sock);
}

static int create_listener(void)
{
    static uv_tcp_t listener;
    struct sockaddr_in addr;
    int r;

    uv_tcp_init(ctx.loop, &listener);
    uv_ip4_addr("0.0.0.0", 7890, &addr);
    if ((r = uv_tcp_bind(&listener, (struct sockaddr *)&addr, 0)) != 0) {
        fprintf(stderr, "uv_tcp_bind:%s\n", uv_strerror(r));
        goto Error;
    }
    if ((r = uv_listen((uv_stream_t *)&listener, 128, on_accept)) != 0) {
        fprintf(stderr, "uv_listen:%s\n", uv_strerror(r));
        goto Error;
    }

    return 0;
Error:
    uv_close((uv_handle_t *)&listener, NULL);
    return r;
}

#else

static void on_accept(h2o_socket_t *listener, const char *err)
{
    h2o_socket_t *sock;

    if (err != NULL) {
        return;
    }

    if ((sock = h2o_evloop_socket_accept(listener)) == NULL)
        return;
    h2o_accept(&accept_ctx, sock);
}

static int create_listener(void)
{
    struct sockaddr_in6 addr;
    int fd, reuseaddr_flag = 1;
    h2o_socket_t *sock;

    memset(&addr, 0, sizeof(addr));
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(7890);

    if ((fd = socket(AF_INET6, SOCK_STREAM, 0)) == -1 ||
        setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_flag, sizeof(reuseaddr_flag)) != 0 ||
        bind(fd, (struct sockaddr *)&addr, sizeof(addr)) != 0 || listen(fd, SOMAXCONN) != 0) {
        return -1;
    }

    sock = h2o_evloop_socket_create(ctx.loop, fd, H2O_SOCKET_FLAG_DONT_READ);
    h2o_socket_read_start(sock, on_accept);

    return 0;
}
#endif

static int setup_ssl(const char *ca_file, const char *cert_file, const char *key_file, const char *ciphers)
{
    init_openssl();

    accept_ctx.ssl_ctx = SSL_CTX_new(TLS_server_method());

    if (USE_MEMCACHED) {
        accept_ctx.libmemcached_receiver = &libmemcached_receiver;
        h2o_accept_setup_memcached_ssl_resumption(h2o_memcached_create_context("0.0.0.0", 11211, 0, 1, "h2o:ssl-resumption:"),
                                                  86400);
        h2o_socket_ssl_async_resumption_setup_ctx(accept_ctx.ssl_ctx);
    }

    /* load certificate and private key */
    if (SSL_CTX_load_verify_locations(accept_ctx.ssl_ctx, ca_file, NULL) != 1) {
        fprintf(stderr, "an error occurred while trying to load ca certificate file:%s\n", ca_file);
        return -1;
    }

    if (SSL_CTX_use_certificate_file(accept_ctx.ssl_ctx, cert_file, SSL_FILETYPE_PEM) != 1) {
        fprintf(stderr, "an error occurred while trying to load server certificate file:%s\n", cert_file);
        return -1;
    }
    if (SSL_CTX_use_PrivateKey_file(accept_ctx.ssl_ctx, key_file, SSL_FILETYPE_PEM) != 1) {
        fprintf(stderr, "an error occurred while trying to load private key file:%s\n", key_file);
        return -1;
    }

    STACK_OF(X509_NAME) *cert_names;

    if ((cert_names = SSL_load_client_CA_file(ca_file)) == NULL) {
        fprintf(stderr, "an error occurred while trying to load client ca certificate file:%s\n", ca_file);
        return -1;
    }

    SSL_CTX_set_client_CA_list(accept_ctx.ssl_ctx, cert_names);

    if (SSL_CTX_set_cipher_list(accept_ctx.ssl_ctx, ciphers) != 1) {
        fprintf(stderr, "ciphers could not be set: %s\n", ciphers);
        return -1;
    }

    SSL_CTX_set_verify(accept_ctx.ssl_ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
    SSL_CTX_set_verify_depth(accept_ctx.ssl_ctx, 1);
    SSL_CTX_set_min_proto_version(accept_ctx.ssl_ctx, TLS1_2_VERSION);
    SSL_CTX_build_cert_chain(accept_ctx.ssl_ctx, SSL_BUILD_CHAIN_FLAG_NO_ROOT);
    SSL_CTX_set_ciphersuites(accept_ctx.ssl_ctx, "TLS_CHACHA20_POLY1305_SHA256+TLS_AES_256_GCM_SHA384+TLS_AES_128_GCM_SHA256");

    struct st_h2o_quic_resumption_args_t quic_args_buf = {}, *quic_args = NULL;
    h2o_barrier_t *sync_barrier = NULL;
    ssl_setup_session_resumption(&accept_ctx.ssl_ctx, 1, quic_args, sync_barrier);

/* setup protocol negotiation methods */
#if H2O_USE_NPN
    h2o_ssl_register_npn_protocols(accept_ctx.ssl_ctx, h2o_http2_npn_protocols);
#endif
#if H2O_USE_ALPN
    h2o_ssl_register_alpn_protocols(accept_ctx.ssl_ctx, h2o_http2_alpn_protocols);
#endif

    return 0;
}

int tuitta_run_server(PGconn *conn)
{
    h2o_hostconf_t *hostconf;
    h2o_access_log_filehandle_t *logfh = h2o_access_log_open_handle("/dev/stdout", NULL, H2O_LOGCONF_ESCAPE_APACHE);
    h2o_pathconf_t *pathconf;

    signal(SIGPIPE, SIG_IGN);

    h2o_config_init(&config);
    hostconf = h2o_config_register_host(&config, h2o_iovec_init(H2O_STRLIT("default")), 65535);

    pathconf = register_handler(hostconf, "/api/timeline", api_timeline);
    if (logfh != NULL)
      h2o_access_log_register(pathconf, logfh);
    pathconf = register_handler(hostconf, "/api/update", api_update);
    if (logfh != NULL)
      h2o_access_log_register(pathconf, logfh);
    pathconf = register_handler(hostconf, "/api/search", api_search);
    if (logfh != NULL)
      h2o_access_log_register(pathconf, logfh);
    pathconf = register_handler(hostconf, "/api/date", api_date);
    if (logfh != NULL)
      h2o_access_log_register(pathconf, logfh);
    pathconf = h2o_config_register_path(hostconf, "/", 0);
    h2o_file_register(pathconf, "doc_root", NULL, NULL, 0);
    if (logfh != NULL)
      h2o_access_log_register(pathconf, logfh);

#if H2O_USE_LIBUV
    uv_loop_t loop;
    uv_loop_init(&loop);
    h2o_context_init(&ctx, &loop, &config);
#else
    h2o_context_init(&ctx, h2o_evloop_create(), &config);
#endif
    if (USE_MEMCACHED)
        h2o_multithread_register_receiver(ctx.queue, &libmemcached_receiver, h2o_memcached_receiver);

    if (USE_HTTPS && setup_ssl("ca.crt", "server.crt", "server.key",
                               "DEFAULT:!MD5:!DSS:!DES:!RC4:!RC2:!SEED:!IDEA:!NULL:!ADH:!EXP:!SRP:!PSK") != 0)
        goto Error;

    accept_ctx.ctx = &ctx;
    accept_ctx.hosts = config.hosts;

    if (create_listener() != 0) {
        fprintf(stderr, "failed to listen to [::]:7890:%s\n", strerror(errno));
        goto Error;
    }
    
    db_conn = conn;

#if H2O_USE_LIBUV
    uv_run(ctx.loop, UV_RUN_DEFAULT);
#else
    while (h2o_evloop_run(ctx.loop, INT32_MAX) == 0)
        ;
#endif

Error:
    return 1;
}
