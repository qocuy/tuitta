window.addEventListener('load', function onLoad () {
  Search.init();
  Form.init();
  Timeline.init();
  Paginator.init();
  Menu.init();
  Menu.select('home');
});

function callAPI(method, path, data, onSuccess, retryCount) {
  var req = new XMLHttpRequest();

  req.addEventListener('loadend', function () {
    if (req.status == 200) {
      onSuccess(JSON.parse(req.responseText));
    } else {
      if (retryCount == 0) {
        alert('error');
      } else {
        callAPI(method, path, data, onSuccess, retryCount - 1);
      }
    }
  });

  req.open(method, path);
  req.send(data);
}

function postAPI(path, data, onSuccess) {
  callAPI('POST', path, data, onSuccess, 3);
}

function getAPI(path, onSuccess) {
  callAPI('GET', path, null, onSuccess, 3);
}

var Menu = {}

Menu.items = {};

Menu.init = function () {
  Menu.add('home', 'Home',
    function () {
      Paginator.setLoader(Timeline.loadTimeline);
    },
    function () {
      Timeline.clear();
    });
  Menu.add('search', 'Search',
    function (query) {
      document.getElementById('search-form').style.display = 'block';
      if (query) {
        document.getElementById('search-query').value = query;
        Search.submit();
      }
    },
    function () {
      document.getElementById('search-form').style.display = 'none';
      Timeline.clear();
    });
  Menu.add('archives', 'Archives',
    function (date) {
      if (date) Archives.showDate(date);
    },
    function () {
      Timeline.clear();
    });
};

Menu.add = function (name, label, show, hide) {
  var elem = document.createElement('span');
  elem.addEventListener('click', function () {
    if (Menu.selected != name) Menu.select(name);
  });
  elem.textContent = label;
  var menu = document.getElementById('menu');
  menu.appendChild(elem);
  menu.appendChild(document.createTextNode(' '));
  Menu.items[name] = {elem: elem, show: show, hide: hide};
};

Menu.select = function (name, param) {
  if (Menu.selected) {
    Menu.items[Menu.selected].hide();
    Menu.items[Menu.selected].elem.classList.remove('menu-selected');
  }

  Menu.items[name].show(param);
  Menu.items[name].elem.classList.add('menu-selected');
  Menu.selected = name;
};

var Search = {};

Search.init = function () {
  var form = document.getElementById('search-form');

  form.addEventListener('submit', function (e) {
    e.preventDefault();
    Search.submit();
  });
}

Search.submit = function () {
  var input = document.getElementById('search-query');
  Search.query = input.value;
  Paginator.setLoader(Search.loadSearchResult);
};

Search.loadSearchResult = function (page, callback) {
  postAPI('/api/search?' + page, Search.query, callback);
};

var Archives = {};

Archives.showDate = function (date) {
  Archives.date = date;
  Paginator.setLoader(Archives.loadDate);
};

Archives.loadDate = function (page, callback) {
  postAPI('/api/date?' + page, Archives.date, callback);
};

var Form = {};

Form.init = function () {
  var form = document.getElementById('tweet-form');
  var input = document.getElementById('tweet-input');

  input.addEventListener('input', function () {
    input.style.height = '16px';
    input.style.height = input.scrollHeight + 'px';
    console.log(input.scrollHeight);
  });

  input.addEventListener('keydown', function (e) {
    if (e.ctrlKey && e.keyCode == 13) {
      Form.submit();
    }
  });

  form.addEventListener('submit', function (e) {
    e.preventDefault();
    Form.submit();
  });
}

Form.submit = function () {
  if (Form.sending) return;

  var input = document.getElementById('tweet-input');

  if (input.value == '') return;

  var submit = document.getElementById('tweet-submit');

  Form.sending = true;
  submit.disabled = true;
  input.readonly = true;

  postAPI('/api/update', input.value, function (tweet) {
    window.requestAnimationFrame(function () {
      input.value = '';
      input.style.height = '24px';
      Form.sending = false;
      submit.disabled = false;
      input.readonly = false;

      if (Menu.selected == 'home' && Paginator.page == 0) {
        Timeline.appendTweet(tweet);
      }

      input.focus();
    });
  });
}

var Paginator = {};

Paginator.init = function () {
  document.getElementById('first')
    .addEventListener('click',Paginator.first);

  document.getElementById('prev')
    .addEventListener('click', Paginator.prev);

  document.getElementById('next')
    .addEventListener('click', Paginator.next);
}

Paginator.first = function () {
  if (Paginator.page > 0) Paginator.goTo(0);
};

Paginator.prev = function () {
  if (Paginator.page > 0) Paginator.goTo(Paginator.page - 1);
};

Paginator.next = function () {
  if (Timeline.showingTweets == 20) Paginator.goTo(Paginator.page + 1);
}

Paginator.goTo = function (num) {
  Paginator.loader(num, Timeline.show);
  Paginator.page = num;
  document.getElementById('page').textContent = 'page: ' + num;
};

Paginator.setLoader = function (loader) {
  Paginator.loader = loader;
  Paginator.goTo(0);
};

var Timeline = {};

Timeline.init = function () {
  Timeline.elem = document.getElementById('timeline');
};

Timeline.clear = function () {
  var timeline = Timeline.elem;
  var clone = timeline.cloneNode(false);
  timeline.parentNode.replaceChild(clone, timeline);
  Timeline.elem = clone;
  Timeline.showingTweets = 0;
  Timeline.lastDate = null;
}

Timeline.shift = function () {
  var timeline = Timeline.elem;

  if (timeline.childNodes[2].className == 'date') {
    timeline.removeChild(timeline.firstChild);
    timeline.removeChild(timeline.firstChild);
  } else {
    timeline.removeChild(timeline.childNodes[1]);
  }

  Timeline.showingTweets--;
}

Timeline.appendTweet = function (tweet) {
  Timeline.drawTweets(Timeline.elem, [tweet]);
  if (Timeline.showingTweets == 20) {
    Timeline.shift();
  }
  Timeline.showingTweets += 1;
}

Timeline.showingTweets = 0;

Timeline.createTweetElement = function (tweet) {
  var elem = document.createElement('div');
  var text = document.createElement('span');
  text.className = 'tweet-text';
  text.textContent = tweet['text'];
  autoLink(text);
  var time = document.createElement('span');
  time.className = 'tweet-time';
  time.textContent = Timeline.formatTime(tweet['createdAt']);
  elem.appendChild(text);
  elem.appendChild(document.createTextNode(' '));
  elem.appendChild(time);
  return elem;
}

Timeline.formatTime = function (createdAt) {
  var hours = createdAt.getHours();
  var minutes = createdAt.getMinutes();
  var seconds = createdAt.getSeconds();
  return [hours, minutes, seconds].map(zeroPad).join(':');
};

Timeline.formatDate = function (createdAt) {
  var year = createdAt.getFullYear();
  var month = zeroPad(createdAt.getMonth() + 1);
  var day = zeroPad(createdAt.getDate());
  return year + '/' + month + '/' + day;
}

Timeline.createDateElement = function (date) {
  var elem = document.createElement('h2');
  elem.textContent = date;
  elem.className = 'date';
  elem.addEventListener('click', function () {
    Menu.select('archives', this.textContent);
  });
  return elem;
}

Timeline.parseDate = function (date) {
  return new Date(date.replace(/-/g, '/').replace(/\.\d+$/, ''));
};

Timeline.drawTweets = function (container, tweets) {
  for (var i = 0; i < tweets.length; i++) {
    var tweet = tweets[i];
    tweet['createdAt'] = Timeline.parseDate(tweet['createdAt']);
    var date = Timeline.formatDate(tweet['createdAt']);

    if (date != Timeline.lastDate) {
      Timeline.lastDate = date;
      var dateElement = Timeline.createDateElement(date);
      container.appendChild(dateElement);
    }

    var tweetElement = Timeline.createTweetElement(tweet);
    container.appendChild(tweetElement);
  }
}

Timeline.show = function (tweets) {
  var newTimeline = Timeline.elem.cloneNode(false);
  Timeline.lastDate = null;
  Timeline.drawTweets(newTimeline, tweets);
  Timeline.elem.parentNode.replaceChild(newTimeline, Timeline.elem);
  Timeline.elem = newTimeline;
  Timeline.showingTweets = tweets.length;
}

Timeline.loadTimeline = function (page, callback) {
  getAPI('/api/timeline?' + page, callback);
}

function getText(node) {
  if (node.nodeType == Node.TEXT_NODE) {
    return node.nodeValue;
  }

  return node.textContent;
}

function setText(node, text) {
  if (node.nodeType == Node.TEXT_NODE) {
    node.nodeValue = text;
    return;
  }

  node.textContent = text;
}

function splitNode(parent, node, index) {
  if (node.nodeType == Node.TEXT_NODE) {
    return node.splitText(index);
  }

  var splitted = node.firstChild.splitText(index);
  var newNode = node.cloneNode();
  node.removeChild(splitted);
  newNode.appendChild(splitted);
  parent.insertBefore(newNode, node.nextSibling);
  return newNode;
}

function createLink(parent, url, chain) {
  var link = document.createElement('a');
  link.href = url;
  link.target = '_blank';

  var before = chain[chain.length - 1][0].nextSibling;

  for (var j = 0; j < chain.length; j++) {
    parent.removeChild(chain[j][0]);
    link.appendChild(chain[j][0]);
  }

  parent.insertBefore(link, before);

  return link;
}

function autoLink (tweet) {
  var chain = [];
  var textChain = '';
  var node = tweet.firstChild

  while (node) {
    var text = getText(node);
    chain.push([node, text.length]);
    textChain += text;

    if (match = textChain.match(/https?:\/\/[\w\/:%#\$&\?\(\)~\.=\+\-]+/)) {
      var url = match[0];
      // 先頭のURL以外の文字列、ノードを削除
      if (match.index > 0) {
        var textIndex = 0;
        var i = 0;
        while (textIndex + chain[i][1] < match.index) {
          textIndex += chain[i][1];
          i++;
        }
        //console.log('chainH', chain.map(function (x) { return getText(x[0]) }))
        if (match.index - textIndex < chain[i][1]) {
          var splitLength = match.index - textIndex;
          var splittedNode = splitNode(tweet, chain[i][0], splitLength);
          chain.splice(i + 1, 0, [splittedNode, chain[i][1] - splitLength]);
          //console.log('afterH', chain.map(function (x) { return getText(x[0]) }));
        }
        chain.splice(0, i + 1);
        //console.log('splicedH', chain.map(function (x) { return getText(x[0]) }))
        textChain = textChain.slice(match.index);
        node = chain[chain.length - 1][0];
      }

      // 末尾のURL以外の文字列、ノードを削除
      if (url.length < textChain.length) {
        var textIndex = 0;
        var i = 0;
        while (textIndex + chain[i][1] < url.length) {
          textIndex += chain[i][1];
          i++;
        }
        //console.log('chainT', chain.map(function (x) { return getText(x[0]) }))
        if (textIndex + chain[i][1] > url.length) {
          var splittedLength = textChain.length - url.length;
          var newLength = chain[i][1] - splittedLength;
          splitNode(tweet, chain[i][0], newLength);
          //console.log('afterT', chain.map(function (x) { return getText(x[0]) }))
        }
        chain.splice(i + 1);
        //console.log('splicedT', chain.map(function (x) { return getText(x[0]) }))
        node = createLink(tweet, url, chain);
        textChain = '';
        chain = [];
      } else if (!node.nextSibling) {
        createLink(tweet, textChain, chain);
      }
    }
    node = node.nextSibling;
  }
}

function zeroPad (num) {
  return ('0' + num).slice(-2);
}
