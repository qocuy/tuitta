#include <postgresql/libpq-fe.h>

PGresult *tuitta_db_get_timeline(PGconn *conn, const char *count, const char *page) {
  const char *query = "SELECT id, text, created_at + interval '9 hours', has_images FROM tweets ORDER BY id DESC OFFSET ($1::integer * $2) LIMIT $2";
  const char *params[] = {page, count};
  return PQexecParams(conn, query, 2, NULL, params, NULL, NULL, 0);
}

PGresult *tuitta_db_update_status(PGconn *conn, const char *status) {
  const char *query = "INSERT INTO tweets (text, created_at) VALUES ($1, CURRENT_TIMESTAMP - interval '9 hours') RETURNING id, text, created_at + interval '9 hours'";
  return PQexecParams(conn, query, 1, NULL, &status, NULL, NULL, 0);
}

PGresult *tuitta_db_search(PGconn *conn, const char *pattern, const char *count, const char *page) {
  const char *query = "SELECT id, text, created_at + interval '9 hours', has_images FROM tweets WHERE text SIMILAR TO $1 ORDER BY id DESC OFFSET ($2::integer * $3) LIMIT $3";
  const char *params[] = {pattern, page, count};
  return PQexecParams(conn, query, 3, NULL, params, NULL, NULL, 0);
}

PGresult *tuitta_db_travel(PGconn *conn, const char *date, const char *count, const char *page) {
  const char *query = "SELECT id, text, created_at + interval '9 hours', has_images FROM tweets WHERE created_at BETWEEN $1::date - interval '9 hours' AND $1::date + interval '15 hours' ORDER BY id DESC OFFSET ($2::integer * $3) LIMIT $3";
  const char *params[] = {date, page, count};
  return PQexecParams(conn, query, 3, NULL, params, NULL, NULL, 0);
}

PGconn *tuitta_db_open(const char *host, const char *db_name, const char *user, const char *pass) {
  PGconn *conn = PQsetdbLogin(
    host,
    NULL,
    NULL,
    NULL,
    db_name,
    user,
    pass
  );

  if (PQstatus(conn) == CONNECTION_BAD) {
    fprintf(stderr, "%s\n", PQerrorMessage(conn));
    PQfinish(conn);
    return NULL;
  }
  
  return conn;
}

void tuitta_db_close(PGconn *conn) {
  PQfinish(conn);
}
