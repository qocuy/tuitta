#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <postgresql/libpq-fe.h>
#include <readline/readline.h>
#include <signal.h>
#include "config.h"
#include "db.h"
#include "server.h"

static void show_tweets(PGresult *result) {
  ExecStatusType status = PQresultStatus(result);

  if (status != PGRES_TUPLES_OK) {
    fprintf(stderr, "error: %s\n", PQresStatus(status));
    return;
  }

  int tuples = PQntuples(result);

  for (int i = tuples - 1; i >= 0; i--) {
    printf("%s  (%s)\n", PQgetvalue(result, i, 1), PQgetvalue(result, i, 2));
  }
}

static void command_timeline(PGconn *conn) {
  PGresult *result = tuitta_db_get_timeline(conn, TUITTA_DEFAULT_COUNT, TUITTA_PAGE_0);
  show_tweets(result);
  PQclear(result);
}

static void command_update(PGconn *conn, const char *status) {
  PGresult *result = tuitta_db_update_status(conn, status);
  PQclear(result);
}

static void command_travel(PGconn *conn, const char *date) {
  PGresult *result = tuitta_db_travel(conn, date, TUITTA_ALL, TUITTA_PAGE_0);
  show_tweets(result);
  PQclear(result);
}

static void command_search(PGconn *conn, const char *pattern) {
  PGresult *result = tuitta_db_search(conn, pattern, TUITTA_ALL, TUITTA_PAGE_0);
  show_tweets(result);
  PQclear(result);
}

static void cli_interrupt(int sig) {
  exit(0);
}

static void command_interactive(PGconn *conn) {
  signal(SIGINT, cli_interrupt);
  command_timeline(conn);
  char *text;
  for (;;) {
    text = readline("");
    printf("\033[1A");
    if (*text != '\0') {
      PGresult *result = tuitta_db_update_status(conn, text);
      printf("\033[%zuG  (%s)\n", strlen(text), PQgetvalue(result, 0, 2));
      PQclear(result);
    } else {
      printf("\033[0J");
    }
  }
}

int main(int argc, char **argv) {
  PGconn *conn = tuitta_db_open(DB_HOST, DB_NAME, DB_USER, DB_PASS);

  if (argc == 3) {
    if (strcmp(argv[1], "update") == 0) {
      command_update(conn, argv[2]);
    } else if (strcmp(argv[1], "travel") == 0) {
      command_travel(conn, argv[2]);
    } else if (strcmp(argv[1], "search") == 0) {
      command_search(conn, argv[2]);
    }
  } else if (argc == 2 && strcmp(argv[1], "timeline") == 0) {
    command_timeline(conn);
  } else if (argc == 2 && strcmp(argv[1], "server") == 0) {
    tuitta_run_server(conn);
  } else {
    command_interactive(conn);
  }

  PQfinish(conn);

  return 0;
}
