#ifndef _TUITTA_DB_H
#define _TUITTA_DB_H

PGresult *tuitta_db_get_timeline(PGconn *conn, const char *count, const char *page);
PGresult *tuitta_db_update_status(PGconn *conn, const char *status);
PGresult *tuitta_db_search(PGconn *conn, const char *pattern, const char *count, const char *page);
PGresult *tuitta_db_travel(PGconn *conn, const char *date, const char *count, const char *page);
PGconn *tuitta_db_open(const char *host, const char *db_name, const char *user, const char *pass);

#define TUITTA_PAGE_0 "0"
#define TUITTA_ALL "ALL"
#define TUITTA_DEFAULT_COUNT "20"

#endif
